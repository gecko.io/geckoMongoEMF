@org.osgi.annotation.versioning.Version("3.0.0")
@org.osgi.annotation.bundle.Capability(
		namespace = org.gecko.emf.mongo.Keywords.CAPABILITY_NAMESPACE,
		name = org.gecko.emf.mongo.Keywords.CAPABILITY_NAME,
		version = org.gecko.emf.mongo.Keywords.CAPABILITY_VERSION
		)
package org.gecko.emf.mongo.codecs;
