/**
 * Copyright (c) 2012 - 2017 Data In Motion and others.
 * All rights reserved. 
 * 
 * This program and the accompanying materials are made available under the terms of the 
 * Eclipse Public License v1.0 which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Data In Motion - initial API and implementation
 */
package org.gecko.mongo.osgi.tests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import java.io.IOException;
import java.util.Dictionary;
import java.util.Hashtable;

import org.gecko.core.tests.AbstractOSGiTest;
import org.gecko.core.tests.ServiceChecker;
import org.gecko.mongo.osgi.MongoClientProvider;
import org.gecko.mongo.osgi.MongoDatabaseProvider;
import org.gecko.mongo.osgi.configuration.ConfigurationProperties;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;
import org.osgi.framework.BundleException;
import org.osgi.framework.FrameworkUtil;
import org.osgi.framework.InvalidSyntaxException;
import org.osgi.service.cm.Configuration;

import com.mongodb.client.MongoDatabase;

/**
 * Tests the database provider
 * @author Mark Hoffmann
 * @since 25.07.2017
 */
@RunWith(MockitoJUnitRunner.class)
public class MongoDatabaseProviderTest extends AbstractOSGiTest {

	private String mongoHost = System.getProperty("mongo.host", "localhost");
	
	/**
	 * Creates a new instance.
	 */
	public MongoDatabaseProviderTest() {
		super(FrameworkUtil.getBundle(MongoDatabaseProviderTest.class).getBundleContext());
	}

	/* 
	 * (non-Javadoc)
	 * @see org.gecko.core.tests.AbstractOSGiTest#doBefore()
	 */
	@Override
	public void doBefore() {
	}

	/* 
	 * (non-Javadoc)
	 * @see org.gecko.core.tests.AbstractOSGiTest#doAfter()
	 */
	@Override
	public void doAfter() {
	}

	@Test
	public void testNoMongoClientProvider() throws InvalidSyntaxException, BundleException, IOException, InterruptedException {

		// add service properties
		String dbAlias = "testDB";
		String db = "test";
		Dictionary<String, Object> p = new Hashtable<String, Object>();
		p.put(MongoDatabaseProvider.PROP_ALIAS, dbAlias);
		p.put(MongoDatabaseProvider.PROP_DATABASE, db);
		createConfigForCleanup(ConfigurationProperties.DATABASE_PID, "?", p);
		
		createTrackedChecker(MongoClientProvider.class).assertCreations(0, true).immediate(true);
		createTrackedChecker(MongoDatabaseProvider.class).assertCreations(0, false).immediate(true);

		// re-check configuration must not available, because of the missing client
		Configuration clientConfig = getConfigAdmin().getConfiguration(ConfigurationProperties.CLIENT_PID, "?");
		assertNull(clientConfig.getProperties());

	}

	@Test
	public void testCreateMongoDatabaseProvider() throws InvalidSyntaxException, BundleException, IOException, InterruptedException {

		// add service properties
		String clientId = "testClient";
		String clientUri = "mongodb://" + mongoHost + ":27017";
		Dictionary<String, Object> p = new Hashtable<String, Object>();
		p.put(MongoClientProvider.PROP_CLIENT_ID, clientId);
		p.put(MongoClientProvider.PROP_URI, clientUri);
		createConfigForCleanup(ConfigurationProperties.CLIENT_PID, "?", p);

		createTrackedChecker(MongoClientProvider.class).assertCreations(1, true);

		// add service properties
		String dbAlias = "testDB";
		String db = "test";
		Dictionary<String, Object> dbp = new Hashtable<String, Object>();
		dbp.put(MongoDatabaseProvider.PROP_ALIAS, dbAlias);
		dbp.put(MongoDatabaseProvider.PROP_DATABASE, db);
		createConfigForCleanup(ConfigurationProperties.DATABASE_PID, "?", dbp);
		
		ServiceChecker<MongoDatabaseProvider> dbSC = createTrackedChecker(MongoDatabaseProvider.class, true).assertCreations(1, true);

		MongoDatabaseProvider databaseProvider = dbSC.getTrackedService();
		MongoDatabase database = databaseProvider.getDatabase();
		assertEquals(db, database.getName());
	}

}
