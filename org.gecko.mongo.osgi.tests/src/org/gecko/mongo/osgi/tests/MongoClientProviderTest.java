/**
 * Copyright (c) 2012 - 2017 Data In Motion and others.
 * All rights reserved. 
 * 
 * This program and the accompanying materials are made available under the terms of the 
 * Eclipse Public License v1.0 which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Data In Motion - initial API and implementation
 */
package org.gecko.mongo.osgi.tests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.io.IOException;
import java.util.Dictionary;
import java.util.Hashtable;

import org.gecko.core.tests.AbstractOSGiTest;
import org.gecko.core.tests.ServiceChecker;
import org.gecko.mongo.osgi.MongoClientProvider;
import org.gecko.mongo.osgi.configuration.ConfigurationProperties;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;
import org.osgi.framework.BundleException;
import org.osgi.framework.FrameworkUtil;
import org.osgi.framework.InvalidSyntaxException;
import org.osgi.service.cm.Configuration;

/**
 * Tests the mongo client provider
 * @author Mark Hoffmann
 * @since 25.07.2017
 */
@RunWith(MockitoJUnitRunner.class)
public class MongoClientProviderTest extends AbstractOSGiTest {

	private String mongoHost = System.getProperty("mongo.host", "localhost");

	/**
	 * Creates a new instance.
	 */
	public MongoClientProviderTest() {
		super(FrameworkUtil.getBundle(MongoClientProviderTest.class).getBundleContext());
	}

	/* 
	 * (non-Javadoc)
	 * @see org.gecko.core.tests.AbstractOSGiTest#doBefore()
	 */
	@Override
	public void doBefore() {
	}

	/* 
	 * (non-Javadoc)
	 * @see org.gecko.core.tests.AbstractOSGiTest#doAfter()
	 */
	@Override
	public void doAfter() {
	}

	@Test
	public void testCreateMongoClientProvider() throws InvalidSyntaxException, BundleException, IOException, InterruptedException {

		// add service properties
		String clientId = "testClient";
		String clientUri = "mongodb://" + mongoHost + ":27017";
		Dictionary<String, Object> p = new Hashtable<String, Object>();
		p.put(MongoClientProvider.PROP_CLIENT_ID, clientId);
		p.put(MongoClientProvider.PROP_URI, clientUri);

		ServiceChecker<MongoClientProvider> mcpSC = createTrackedChecker(MongoClientProvider.class);
		mcpSC.assertCreations(0, false).assertRemovals(0, false);

		Configuration clientConfig = createConfigForCleanup(ConfigurationProperties.CLIENT_PID, "?", p);
		clientConfig.update(p);

		mcpSC.assertCreations(1, true).assertRemovals(0, false);

		MongoClientProvider clientProvider = mcpSC.getTrackedService();
		assertEquals(clientId, clientProvider.getClientId());
		assertEquals(1, clientProvider.getURIs().length);
		assertEquals(clientUri, clientProvider.getURIs()[0]);

		mcpSC.assertCreations(1, false).assertRemovals(0, false);
		deleteConfigurationAndRemoveFromCleanup(clientConfig);
		mcpSC.assertCreations(1, false).assertRemovals(1, true).immediate(true);
	}

	@Test
	public void testModifyMongoClientProvider() throws InvalidSyntaxException, BundleException, IOException, InterruptedException {

		// add service properties
		String clientId = "testClient";
		String clientUri = "mongodb://" + mongoHost + ":27017";
		Dictionary<String, Object> p = new Hashtable<String, Object>();
		p.put(MongoClientProvider.PROP_CLIENT_ID, clientId);
		p.put(MongoClientProvider.PROP_URI, clientUri);

		ServiceChecker<MongoClientProvider> mcpSC = createTrackedChecker(MongoClientProvider.class);
		mcpSC.assertCreations(0, false).assertModifications(0, false).assertRemovals(0, false);

		Configuration clientConfig = createConfigForCleanup(ConfigurationProperties.CLIENT_PID, "?", p);
		clientConfig.update(p);

		mcpSC.assertCreations(1, true).assertModifications(0, false).assertRemovals(0, false);

		MongoClientProvider clientProvider = mcpSC.getTrackedService();
		assertEquals(clientId, clientProvider.getClientId());
		assertEquals(1, clientProvider.getURIs().length);
		assertEquals(clientUri, clientProvider.getURIs()[0]);

		// remove configuration
		p = new Hashtable<String, Object>();
		p.put(MongoClientProvider.PROP_CLIENT_ID, clientId + "2");
		p.put(MongoClientProvider.PROP_URI, clientUri);
		
		mcpSC.assertCreations(1, false).assertModifications(0, false).assertRemovals(0, false);
		clientConfig.update(p);
		mcpSC.assertCreations(1, false).assertModifications(1, true).assertRemovals(0, false);
		
		clientProvider = mcpSC.getTrackedService();
		assertNotNull(clientProvider);
		assertEquals(clientId + "2", clientProvider.getClientId());
		assertEquals(1, clientProvider.getURIs().length);
		assertEquals(clientUri, clientProvider.getURIs()[0]);

		mcpSC.assertCreations(1, false).assertModifications(1, false).assertRemovals(0, false);
		deleteConfigurationAndRemoveFromCleanup(clientConfig);
		mcpSC.assertCreations(1, false).assertModifications(1, false).assertRemovals(1, true).immediate(true);
	}

}
