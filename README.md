# Gecko Mongo EMF

This project realizes persistence of EMF objects into a MongoDB using OSGi. It is based on the **Gecko Mongo OSGi** setup, that handles 
configuring a MongoDB client and databases in OSGi.

The Mongo OSGi setup just bases on the MongoDB driver. For that it defines a capability requirement on that driver with the bundle symbolic name **org.mongodb.mongo-java-driver**.

You can find the artifacts on Maven central. For Snapshot builds have a look at our Nexus:

https://devel.data-in-motion.biz/nexus/repository/dim-snapshot/

The artifacts you need are:

```
<dependency>
  <groupId>org.geckoprojects.mongo</groupId>
  <artifactId>org.gecko.mongo.osgi.api</artifactId>
  <version>6.0.0</version>
</dependency>
<dependency>
  <groupId>org.geckoprojects.mongo</groupId>
  <artifactId>org.gecko.mongo.osgi.component</artifactId>
  <version>6.0.0</version>
</dependency>
```

From Maven Central you can retrieve the Mongo Java Driver:

```
<dependency>
  <groupId>org.mongodb</groupId>
  <artifactId>mongo-java-driver</artifactId>
  <version>3.9.1</version>
</dependency>
```

We also provide a Maven BOM:

```
<dependency>
  <groupId>org.geckoprojects.mongo</groupId>
  <artifactId>org.gecko.mongo.osgi.bom</artifactId>
  <version>6.0.0</version>
</dependency>
```


## Mongo OSGi

There are two configurable components that create   

`MongoClientProvider`

`MongoDatabaseProvider`

instances. The first is to create a MongoDB client, the second to get a Mongo database.

Additionally there is an interface:

`MongoIdFactory`

to create id's for the MongoDB. 

The following bundles are necessary to run Gecko Mongo OSGi:

* **org.gecko.mongo.osgi.api** 
* **org.gecko.mongo.osgi.component** 

### Create a Mongo Client Provider

To create a client provider you can either use the ConfigurationAdmin or the OSGi Configurator to setup all information.

```
ConfigurationAdmin cm = ...;

Configuration clientConfigruation = cm.getConfiguration(ConfigurationProperties.CLIENT_PID, "?");

String clientId = "testClient";
String clientUri = "mongodb://localhost:27017";
Dictionary<String, Object> p = new Hashtable<String, Object>();
p.put(MongoClientProvider.PROP_CLIENT_ID, clientId);
p.put(MongoClientProvider.PROP_URI, clientUri);

clientConfigruation.update(p);

```

To setup a client for multiple replica set instance, the server adresses can be concatenated as comma separated value:

```
String clientUri = "mongodb://mongodb1:27017,mongodb://mongodb2:27017,mongodb://mongodb3:27017";

```

There are a lot of client options of the MongoClient, that are supported as properties as well. 

The supported properties are described in `org.gecko.mongo.osgi.MongoClientProvider`

### Mongo Client Provider Authentication

In the current version of the MongoDB the user management switched from the *MongoDatabase* to the *MongoClient*. Use the property *MongoClientProvider.PROP_CREDENTIALS* for that. When using the ConfigurationAdmin this property is hidden, because it starts with a dot, so no one can inspect the credentials during runtime.

The pattern to define user credentials is, as well comma separated, like this: *user:password@dbname,user2:password2@db2*

```
ConfigurationAdmin cm = ...;

Configuration clientConfigruation = cm.getConfiguration(ConfigurationProperties.CLIENT_PID, "?");

String clientId = "testClient";
String clientUri = "mongodb://localhost:27017";
Dictionary<String, Object> p = new Hashtable<String, Object>();
p.put(MongoClientProvider.PROP_CREDENTIALS, "emil:emilspwd@testdb,test:testpwd@mydb");
p.put(MongoClientProvider.PROP_URI, clientUri);

clientConfigruation.update(p);

```

### Create Mongo Database Provider

To create a database provider, a MongoClientProvider instance is needed. It is a mandatory reference for the database provider.
Beside that you can setup like this:

```
ConfigurationAdmin cm = ...;

Configuration clientConfiguration = cm.getConfiguration(ConfigurationProperties.CLIENT_PID, "?");

String clientId = "testClient";
String clientUri = "mongodb://localhost:27017";
Dictionary<String, Object> p = new Hashtable<String, Object>();
p.put(MongoClientProvider.PROP_CLIENT_ID, clientId);
p.put(MongoClientProvider.PROP_URI, clientUri);

clientConfiguration.update(p);

Configuration databaseConfiguration = cm.getConfiguration(ConfigurationProperties.DATABASE_PID, "?");

String dbAlias = "testDB";
String db = "test";
Dictionary<String, Object> dbp = new Hashtable<String, Object>();
dbp.put(MongoDatabaseProvider.PROP_ALIAS, dbAlias);
dbp.put(MongoDatabaseProvider.PROP_DATABASE, db);

databaseConfiguration.update(dbp);

```

To use a special client provider instance for this database provider you can define the client target filter. 

The example uses a client with a MongoClientProvider with *MongoClientProvider.PROP_CLIENT_ID = "my-test"*. 

```
ConfigurationAdmin cm = ...;

Configuration databaseConfiguration = cm.getConfiguration(ConfigurationProperties.DATABASE_PID, "?");

String dbAlias = "testDB";
String db = "test";
Dictionary<String, Object> dbp = new Hashtable<String, Object>();
dbp.put(MongoDatabaseProvider.PROP_ALIAS, dbAlias);
dbp.put(MongoDatabaseProvider.PROP_DATABASE, db);
dbp.put(MongoDatabaseProvider.PROP_CLIENT_FILTER, "(" + MongoClientProvider.PROP_CLIENT_ID + "=my-test)")

databaseConfiguration.update(dbp);

```

### Mongo ID Factory

If there is a need to customize the generation of ID's for the MongoDB, an instance of *MongoIdFactory* can be created. As default a component factory is created the creates *org.bson.types.ObjectId.ObjectId()* for all collections.

To customize this, an alternative implementation can be mad an registered as service.

### Using OSGi Configurator

Instead of defining everything by hand using OSGi's ConfigurationAdmin, you can alternativley use the OSGi Configurator, that does everything, using a Json configuration like this:

```json
{
  ":configurator:resource-version": 1,
  
  "MongoClientProvider": 
  {
    "client_id": "demoClient",
    "uri" : "mongodb://localhost:27017"
  },
  "MongoDatabaseProvider":
  {
    "alias": "demoDB",
    "database" : "demo",
    "MongoClientProvider.target" : "(client_id=demoClient)"
  }
}
```

But remember the Configurator specification!!! Don't forget to package the config file within the resulting bundle jar. Also mention the configurator capability in the bundle manifest: 

```
Require-Capability: osgi.extender;filter:="(osgi.extender=osgi.configurator)"
```



## Mongo EMF

On top of the Mongo OSGi setup there is an extension, that enables you to store EMF objects into a MongoDB. This implementation enables EMF to use a URI with the schema **mongodb** to store data in a MongoDB via *Resource#save* and loading data using *Resource#load*.

For that, we use an EMF URIHandler implementation, that can deal with that. A typical URI would then like like:

`mongodb://mongohost/my-database/my-collection/my-id-field-value`

As long as you meta-model is registered in your ResourceSet, you then can load EObjects from this resource and as well save them.

### EMF Model Pre-Conditions

To identify an instance in the collections, an id is needed. So ensure you have a field, marked as ID=true in your EClass. Mongo EMF will then take this field as id-field in the MongoDB.

### Artifacts / Dependencies

On top of the Mongo OSGi dependencies, you need the following artifacts. You can find them in our Nexus:

https://devel.data-in-motion.biz/nexus/repository/maven-public/

The artifacts you need are:

```
<dependency>
  <groupId>org.gecko.emf.mongo</groupId>
  <artifactId>org.gecko.emf.mongo.api</artifactId>
  <version>5.2.3</version>
</dependency>
<dependency>
  <groupId>org.gecko.emf.mongo</groupId>
  <artifactId>org.gecko.emf.mongo.component</artifactId>
  <version>4.2.3</version>
</dependency>
<dependency>
  <groupId>org.gecko.emf</groupId>
  <artifactId>org.gecko.emf.osgi.api</artifactId>
  <version>3.3.1</version>
</dependency>
```

### Setting up Mongo EMF

Mongo EMF will need a registered `MongoDatabaseProvider` and `MongoClientProvider`service. It will then register a service  `ResourceSetConfigurator` for this MongoDB setup. This ResourceSetConfigurator comes out of the EMF OSGi project. 

Usually your EMF models are registered using the extension points in Eclipse, or you use Gecko EMF OSGi, that provides a registration without extension points. A third way id registering EPackages by hand:

```java
ResourceSet rs = new ResourceSetImpl();
rs.getPackageRegistry().put(EcorePackage.eNS_URI, EcorePackage.eINSTANCE);
rs.getPackageRegistry().put(DemoPackage.eNS_URI, DemoPackage.eINSTANCE);
		rs.getResourceFactoryRegistry().getExtensionToFactoryMap().put("demo", new DemoResourceFactoryImpl());
rs.getResourceFactoryRegistry().getExtensionToFactoryMap().put("*", new XMIResourceFactoryImpl());
```

Once you have a ResourceSet with you model registered, you only need to configure it using the ResourceSetConfigurator, that is provided by MongoEMF. You can inject this service into DS components like this:

```java
@Reference(target = "(&(emf.configurator.name=mongo)(database.alias=<my-database-alias))")
private ResourceSetConfigurator configurator;
```

We strongly recommend getting this service using the target filter, to ensure, you have the right configurator in hand. With this target you also know, that the configurator is ready for your Mongo database.

Now just call `configurator.configureResourceSet(resourceSet)`

Now your EMF  `ResourceSet` is ready to use. Like you used to, you can create a `Resource`:

```java
URI uri = URI.createURI("mongodb://localhost:27017/demo/Person/123");
Resource r = resourceSet.createResource(uri);
```

This creates an URI for server located at `localhost` for database **demo**, collection **Person** and primary key **123**

```java
Person p = DemoFactory.eINSTANCE.createPerson();
p.setFirstName("Emil");
p.setLastName("Tester");
//p.setPersonId(ObjectId.get().toString()); This is an ID field in our example
URI uri = URI.createURI("mongodb://localhost:27017/demo/Person/");
Resource r = resourceSet.createResource(uri);
r.getContents().add(p);
try {
	r.save(null);
} catch (IOException e) {
	e.printStackTrace();
}
String personId = p.getPersonId();// Now we get the generated MongoDB - ObjectId
```

Because we now have the object stored in the collection **Person** with a generated identifier, we will need this to load this Person again:

```java
URI uri = URI.createURI("mongodb://localhost:27017/demo/Person/" + personId);
Resource lr = resourceSet.createResource(uri);
try {
	lr.load(null);
} catch (IOException e) {
	e.printStackTrace();
}
Person loaded = (Person) lr.getContents().get(0);
```

Remember, if you set the id fields to a value, this id will be taken as primary key in the MongoDB.

```java
Person p = DemoFactory.eINSTANCE.createPerson();
p.setFirstName("Emil");
p.setLastName("Tester");
p.setPersonId("123");
URI uri = URI.createURI("mongodb://localhost:27017/demo/Person/");
Resource r = resourceSet.createResource(uri);
r.getContents().add(p);
try {
	r.save(null);
} catch (IOException e) {
	e.printStackTrace();
}
String personId = p.getPersonId();// Now we use '123' as id in the database
```

### Additional fields in the MongoDB 

If you look into the MongoDB, you will notice additional fields that are automatically stored in the database. Because MongoDB declares fields starting with "_" as internal fields. MongoEMF also uses this semantics

* **_eClass** - The EClass URI. This is used to identify the documents corresponding EClass. Because you are free to store data in any collection you like you will need a discriminator that describes the EClass. 
* **_timestamp** - The timestamp of the creation, used in the `Resource`

Especially the **_eClass** key is customizable. So if you want to substitute it an use maybe **_type** instead, you can do that using load and save options. 

The corresponding option key is:

`org.gecko.emf.mongo.Options.OPTION_KEY_ECLASS_URI`

from the `org.gecko.emf.mongo.api`

```
Map<String, Object> options = new HashMap<String, Object>();
options.put(Options.OPTION_KEY_ECLASS_URI, "_type");
try {
	r.save(options);
	...
	r.load(options);
} catch (IOException e) {
	e.printStackTrace();
}
```



### More Options

We support a lot of useful Options that can be set and used. Have a look at the [org.gecko.emf.mongo.Options](-/blob/develop/org.gecko.emf.mongo/src/org/gecko/emf/mongo/Options.java) for details.
